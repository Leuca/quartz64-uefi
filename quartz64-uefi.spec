%define debug_package %{nil}
%bcond_with debug

%global release_date 2022-07-20
%global release_version %(echo "%{release_date}" | sed 's/-/\./g')

%global platform_commit 3c3b1168017073c2bb2d97336c5929ebae805be1
%global edk2_commit ba0e0e4c6a174b71b18ccd6e47319cc45878893c
%global nonosi_commit 2ac5396ddf24e30fccfa3c72a786b3bae00cd687
%global shortcommit %(c=%{edk2_commit}; echo ${c:0:7})
%global shortcommit_platform %(c=%{platform_commit}; echo ${c:0:7})
%global commit_date 20220829

# Build options
%global TOOL_CHAIN_TAG GCC5
%global TARGET_ARCH AARCH64
%global DEFAULT_KEYS -D DEFAULT_KEYS=TRUE -D PK_DEFAULT_FILE=$WORKSPACE/keys/pk.cer -D KEK_DEFAULT_FILE1=$WORKSPACE/keys/ms_kek.cer -D DB_DEFAULT_FILE1=$WORKSPACE/keys/ms_db1.cer -D DB_DEFAULT_FILE2=$WORKSPACE/keys/ms_db2.cer -D DBX_DEFAULT_FILE1=$WORKSPACE/keys/arm64_dbx.bin
%global BUILD_FLAGS -D SECURE_BOOT_ENABLE=TRUE -D INCLUDE_TFTP_COMMAND=TRUE -D NETWORK_ISCSI_ENABLE=TRUE -D SMC_PCI_SUPPORT=1
%if %{with debug}
%global TARGET DEBUG
%else
%global TARGET RELEASE
%endif

%global RKBIN edk2-rockchip-non-osi/rkbin
%global TRUST_INI RK3568TRUST.ini
%global MINIALL_INI RK3568MINIALL.ini

# Build macros
#

%define BL31 $(grep '^PATH=.*_bl31_' %{RKBIN}/RKTRUST/%{TRUST_INI} | cut -d = -f 2-)

# Build edk
%define buildedk() \
build -n $(nproc) \\\
    -b %{TARGET} \\\
    -a %{TARGET_ARCH} \\\
    -t %{TOOL_CHAIN_TAG} \\\
    -p Platform/Pine64/%{1}/%{1}.dsc \\\
    --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor=L'EDK II for %{1}' \\\
    --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString=L'UEFI for %{1} %{release_date}' \\\
    %{BUILD_FLAGS} %{DEFAULT_KEYS} \
%{nil}

# Build image
%define buildfit() \
    ./scripts/extractbl31.py %{RKBIN}/%{BL31}\
    cp -f Build/%{1}/%{TARGET}_GCC5/FV/RK356X_EFI.fd Build/RK356X_EFI.fd\
    cat uefi.its | sed "s,@BOARDTYPE@,%{2},g" > %{1}_EFI.its\
    /usr/bin/mkimage -f %{1}_EFI.its -E %{1}_EFI.itb\
    rm -f bl31_0x*.bin Build/RK356X_EFI.fd %{1}_EFI.its\
%{nil}

Name:           quartz64-uefi
Version:        %{release_version}

%global WORKSPACE %{_builddir}/quartz64_uefi-%{release_date}

Release:        1%{?dist}
Summary:        Empty metapackage for a port of Tianocore EDK II to Rockchip RK356x based SBCs
License:        BSD-2-Clause-Patent
URL:            http://www.tianocore.org

Source0:        https://github.com/jaredmcneill/quartz64_uefi/archive/%{release_date}/quartz64_uefi-%{release_date}.tar.gz
# Source1 is generated with the build.sh script
Source1:        idblock.bin
Source2:        rk3566-quartz64-a.dtb
Source3:        rk3566-soquartz-cm4.dtb

Patch0:         quartz64-uefi-restore-devicetree-support.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  python3
BuildRequires:  git
BuildRequires:  libuuid-devel
BuildRequires:  acpica-tools
BuildRequires:  openssl
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros
BuildRequires:  python3-pyelftools
BuildRequires:  uboot-tools


%description
This is a port of 64-bit Tiano Core UEFI firmware for Quarz64 boards.

%package -n edk2-pine64-common
Summary:        idblock needed by Quartz64 and SOQuartz boards

%description -n edk2-pine64-common
ID block image used by Pine64 Quartz64 and SOQuarz boards that can be flashed into an SD card or eMMC module (sector 64)

%package -n edk2-quartz64
Summary:        UEFI image for Quartz64 boards

Requires:       edk2-pine64-common = %{version}-%{release}

Conflicts:      uboot-pine64-config

%description -n edk2-quartz64
UEFI image for Quartz64 boards that can be flashed into an SD card or eMMC module (seek 8MiB)

%%package -n edk2-soquartz
Summary:        UEFI image for SOQuartz boards

Requires:       edk2-pine64-common = %{version}-%{release}

Conflicts:      uboot-pine64-config

%description -n edk2-soquartz
UEFI image for SOQuartz boards that can be flashed into an SD card or eMMC module (seek 8MiB)

%prep
%autosetup -n quartz64_uefi-%{release_date} -p1

%build
export WORKSPACE=%{WORKSPACE}

rm -r edk2 edk2-platforms edk2-non-osi

git clone https://github.com/tianocore/edk2.git
pushd edk2
    git checkout %{edk2_commit}
    git submodule update --init
popd

git clone https://github.com/tianocore/edk2-platforms.git
pushd edk2-platforms
    git checkout %{platform_commit}
    git submodule update --init
popd

git clone https://github.com/tianocore/edk2-non-osi.git
pushd edk2-non-osi
    git checkout %{nonosi_commit}
popd

export PACKAGES_PATH=$PWD/edk2:$PWD/edk2-platforms:$PWD/edk2-non-osi:$PWD/edk2-rockchip

. edk2/edksetup.sh

%{__make} -C edk2/BaseTools

# Now before building set up Secure Boot default keys
# From https://github.com/pftf/RPi4/blob/master/.github/workflows/linux_edk2.yml
mkdir keys
openssl req -new -x509 -newkey rsa:2048 -subj "/CN=Pine64 Platform Key/" -keyout /dev/null -outform DER -out keys/pk.cer -days 7300 -nodes -sha256
curl -L https://go.microsoft.com/fwlink/?LinkId=321185 -o keys/ms_kek.cer
curl -L https://go.microsoft.com/fwlink/?linkid=321192 -o keys/ms_db1.cer
curl -L https://go.microsoft.com/fwlink/?linkid=321194 -o keys/ms_db2.cer
curl -L https://uefi.org/sites/default/files/resources/dbxupdate_arm64.bin -o keys/arm64_dbx.bin

# Use package devicetrees
rm -rf rk3566-quartz64-a.dtb
rm -rf rk3566-soquartz-cm4.dtb
install %{SOURCE2} .
install %{SOURCE3} .

# Build for Quartz64
%buildedk Quartz64
%buildfit Quartz64 rk3566-quartz64-a

# Build for SOQuartz
%buildedk SOQuartz
%buildfit SOQuartz rk3566-soquartz-cm4

%install
mkdir -p %{buildroot}%{efi_esp_root}

# Install binaries
install -m 0644 Quartz64_EFI.itb %{buildroot}%{efi_esp_root}
install -m 0644 SOQuartz_EFI.itb %{buildroot}%{efi_esp_root}
install -m 0644 %{SOURCE1} %{buildroot}%{efi_esp_root}

# Text files
install -m 0644 edk2/License.txt .
install -m 0644 edk2/Maintainers.txt .
install -m 0644 edk2/License-History.txt .

%files

%files -n edk2-pine64-common
%{efi_esp_root}/idblock.bin

%files -n edk2-quartz64
%license License.txt
%doc Maintainers.txt README.md License-History.txt
%{efi_esp_root}/Quartz64_EFI.itb

%files -n edk2-soquartz
%license License.txt
%doc Maintainers.txt README.md License-History.txt
%{efi_esp_root}/SOQuartz_EFI.itb

%changelog
* Thu Feb 02 2023 Luca Magrone <luca@magrone.cc> - 2022.07.20-1
- Initial package release
